﻿using ApiMobile.Models;
using ApiMobile.Models.Request;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ApiMobile.Controllers
{
    public class LibroController : ApiController
    {
        private Models.BDLibrosUTEntities db = new Models.BDLibrosUTEntities();

        //GET: api/Libro
        /// <summary>
        /// Obtener Lista de la bd
        /// </summary>
        /// <returns></returns>
        public IQueryable<Libro> GetLibro()
        {
            return db.Libro;
        }

        //GET: api/Libro/1
        /// <summary>
        /// Obtner libro para detalle
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IHttpActionResult GetLibro(int Id)
        {
            Libro libro = db.Libro.Find(Id);
            if (libro == null)
            {
                return NotFound();
            }
            return Ok(libro);
        }

        //POST: api/Libro
        /// <summary>
        /// Crear libro
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Create(LibroVM model)
        {
            var Libro = new Models.Libro();
            if(NombreExist(model.Titulo))
            {
                Libro.Titulo = model.Titulo;
                Libro.Autor = model.Autor;
                Libro.Anio = model.Anio;
                Libro.Editorial = model.Editorial;
                Libro.NumPaginas = model.NumPaginas;
                db.Libro.Add(Libro);
                db.SaveChanges();
                return Ok();
                //return StatusCode(HttpStatusCode.NoContent);
            }
            else
            return BadRequest();
        }

        //PUT: api/Libro/1
        /// <summary>
        /// Editar libro
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="libroVM"></param>
        /// <returns></returns>
        [HttpPut]
        public IHttpActionResult Edit(int Id, LibroVM libroVM)
        {
            Libro model = db.Libro.Find(Id);
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (Id != model.Id)
            {
                return BadRequest();
            }
            model.Titulo = libroVM.Titulo;
            model.Autor = libroVM.Autor;
            model.Anio = libroVM.Anio;
            model.Editorial = libroVM.Editorial;
            model.NumPaginas = libroVM.NumPaginas;
            db.Entry(model).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!LibroeExists(Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);
        }

        //DELETE: api/Libro/1
        /// <summary>
        /// Eliminar libro
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public IHttpActionResult DeleteLibro(int Id)
        {
            Libro model = db.Libro.Find(Id);
            if (model == null)
            {
                return NotFound();
            }

            db.Libro.Remove(model);
            db.SaveChanges();
            return Ok(model);
        }

        /// <summary>
        /// Metodo que valida si el libro ya existe
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        private bool LibroeExists(int Id)
        {
            return db.Libro.Count(e => e.Id== Id) > 0;
        }

        /// <summary>
        /// Metodo que valida el titulo del libro 
        /// </summary>
        /// <returns></returns>
        private bool NombreExist(string Titulo)
        {
            bool response = true;
            if(db.Libro.Any(e => e.Titulo == Titulo))
            {
                response = false;
            }
            return response;
        }
    }
}
