
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/26/2020 17:57:51
-- Generated from EDMX file: C:\Users\livea\source\repos\practica-1-unidad-3\AppMovilBase\ApiMobile\Models\DBLibro.edmx
-- --------------------------------------------------
USE [BDLibrosUT];

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Libro]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Libro];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Libro'
CREATE TABLE [dbo].[Libro] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Titulo] varchar(50)  NULL,
    [Autor] varchar(50)  NULL,
    [Anio] int  NULL,
    [Editorial] varchar(50)  NULL,
    [NumPaginas] int  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Libro'
ALTER TABLE [dbo].[Libro]
ADD CONSTRAINT [PK_Libro]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------