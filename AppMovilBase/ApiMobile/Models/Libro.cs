//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ApiMobile.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Libro
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public Nullable<int> Anio { get; set; }
        public string Editorial { get; set; }
        public Nullable<int> NumPaginas { get; set; }
    }
}
