﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ApiMobile.Models.Request
{
    public class LibroVM
    {
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public int Anio { get; set; }
        public string Editorial { get; set; }
        public int NumPaginas { get; set; }
    }
}