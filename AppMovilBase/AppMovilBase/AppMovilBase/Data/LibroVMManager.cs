﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Android.Net;

namespace AppMovilBase.Data
{
    public class LibroVMManager
    {
        private HttpClient _httpClient;
        public HttpClient HttplicentAccount
        {
            get
            {
                _httpClient = _httpClient ?? new HttpClient
                (
                    new HttpClientHandler()
                    {
                        ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) =>
                        {
                            //bypass
                            return true;
                        },
                    }
                    , false
                )
                {
                    BaseAddress = new Uri("https://192.168.0.5:45455/api/Libro"),
                };
                return _httpClient;
            }
        }

        public async Task<IEnumerable<LibroVM>> GetAll()
        {
            HttpClient client = HttplicentAccount; //con esta linea obtienes la IP de la API
            string result = await client.GetStringAsync(client.BaseAddress);
            return JsonConvert.DeserializeObject<IEnumerable<LibroVM>>(result);
        }

        public async Task<LibroVM> Add(string titulo, string autor, int anio, string editorial, int numPaginas)
        {
            LibroVM libro = new LibroVM()
            {
                Titulo = titulo,
                Autor = autor,
                Anio = anio,
                Editorial = editorial,
                NumPaginas = numPaginas 
            };

            HttpClient client = HttplicentAccount;
            var response = await client.PostAsync(client.BaseAddress, new StringContent(JsonConvert.SerializeObject(libro), Encoding.UTF8,"application/json"));

            return JsonConvert.DeserializeObject<LibroVM>(await response.Content.ReadAsStringAsync());
        }

        public async Task<Response> DeleteLibro(int Id)
        {
            HttpClient client = HttplicentAccount;
            string url = client.BaseAddress + "/DeleteLibro?Id=" + Id;
            HttpResponseMessage response = await client.DeleteAsync(url);
            Response responseReturn = new Response();
            if (response.IsSuccessStatusCode)
            {
                responseReturn.Message = "El libro se eliminó exitosamente";
                responseReturn.Status = 1;
            }
            else
                responseReturn.Message = "Revise que los datos esten bien. Error";
            return responseReturn;
        }

        public async Task<Response> UpdateLibro(int Id, string titulo, string autor, int anio, string editorial, int numPaginas)
        {
            LibroVM libro = new LibroVM()
            {
                Titulo = titulo,
                Autor = autor,
                Anio = anio,
                Editorial = editorial,
                NumPaginas = numPaginas
            };
            HttpClient client = HttplicentAccount;
            string url = client.BaseAddress + "/Edit?Id=" + Id;
            var response = await client.PutAsync(url, new StringContent(JsonConvert.SerializeObject(libro), Encoding.UTF8, "application/json"));
            Response responseReturn = new Response();
            if (response.IsSuccessStatusCode)
            {
                responseReturn.Message = "El libro se edito exitosamente";
                responseReturn.Status = 1;
            }
            else
                responseReturn.Message = "Revise que los datos esten bien. Error";
            return responseReturn;
        }

    }
}
