﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppMovilBase.Data
{
    public class LibroVM
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string Autor { get; set; }
        public int Anio { get; set; }
        public string Editorial { get; set; }
        public int NumPaginas { get; set; }
    }
}
