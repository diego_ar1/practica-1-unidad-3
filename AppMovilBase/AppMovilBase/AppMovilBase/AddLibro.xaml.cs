﻿using AppMovilBase.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace AppMovilBase
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AddLibro : ContentPage
    {

        private LibroVMManager manager;
        private LibroVM libro;

        public AddLibro(LibroVM libro, LibroVMManager manager)
        {
            InitializeComponent();
            this.libro = libro;
            this.manager = manager;
            if (libro.Titulo != null && libro.Autor != null && libro.Anio != 0 && libro.Editorial != null && libro.NumPaginas != 0)
            {
                txtTitulo.Text = libro.Titulo;
                txtAutor.Text = libro.Autor;
                txtAnio.Text = libro.Anio.ToString();
                txtEdit.Text = libro.Editorial;
                txtNpag.Text = libro.NumPaginas.ToString();
                txtId.Text = libro.Id.ToString();
                txtId.IsVisible = false;
                btnGuardar.IsVisible = false;
                btnUpdate.IsVisible = true;
            }
            else
            {
                txtId.IsVisible = false;
                btnGuardar.IsVisible = true;
                btnUpdate.IsVisible = false;
            }
        }

        async public void OnSaveLibro(object sender, EventArgs e)
       {
            if(string.IsNullOrEmpty(txtTitulo.Text))
            {
                await DisplayAlert("Error", "Se requiere el título del libro", "OK");
                return;
            }
            if(string.IsNullOrEmpty(txtAutor.Text))
            {
                await DisplayAlert("Error", "Se requiere el nombre del autor del libro", "OK");
                return;
            }
            if(string.IsNullOrEmpty(txtAnio.Text))
            {
                await DisplayAlert("Error", "Se requiere el año del libro", "OK");
                return;
            }
            if (string.IsNullOrEmpty(txtEdit.Text))
            {
                await DisplayAlert("Error", "Se requiere la editorial del libro", "OK");
                return;
            }
            if (string.IsNullOrEmpty(txtNpag.Text))
            {
                await DisplayAlert("Error", "Se requiere el número de páginas del libro", "OK");
                return;
            }
            try
            {
                await manager.Add(txtTitulo.Text, txtAutor.Text, int.Parse(txtAnio.Text), txtEdit.Text, int.Parse(txtNpag.Text));
                await DisplayAlert("Información", "El libro se agregó correctamente", "OK");
                await Navigation.PopAsync();
            }
            catch(Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
                return;
            }
       }

        async public void UpdateLibro(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtTitulo.Text))
            {
                await DisplayAlert("Error", "Se requiere el título del libro", "OK");
                return;
            }
            if (string.IsNullOrEmpty(txtAutor.Text))
            {
                await DisplayAlert("Error", "Se requiere el nombre del autor del libro", "OK");
                return;
            }
            if (string.IsNullOrEmpty(txtAnio.Text))
            {
                await DisplayAlert("Error", "Se requiere el año del libro", "OK");
                return;
            }
            if (string.IsNullOrEmpty(txtEdit.Text))
            {
                await DisplayAlert("Error", "Se requiere la editorial del libro", "OK");
                return;
            }
            if (string.IsNullOrEmpty(txtNpag.Text))
            {
                await DisplayAlert("Error", "Se requiere el número de páginas del libro", "OK");
                return;
            }
            try
            {
                btnUpdate.IsEnabled = false;
                var response = await manager.UpdateLibro(int.Parse(txtId.Text), txtTitulo.Text, txtAutor.Text, int.Parse(txtAnio.Text), txtEdit.Text, int.Parse(txtNpag.Text));
                if (response.Status == 1)
                {
                    await DisplayAlert("Info", response.Message, "OK");
                    btnUpdate.IsEnabled = true;
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Error", response.Message, "OK");
                    btnUpdate.IsEnabled = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
                btnUpdate.IsEnabled = true;
                return;
            }
        }
    }
}