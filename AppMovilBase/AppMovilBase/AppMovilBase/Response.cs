﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AppMovilBase
{
    public class Response
    {
        public string Message { get; set; }
        public int Status { get; set; }
    }
}
