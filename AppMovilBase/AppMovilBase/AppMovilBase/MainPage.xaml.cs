﻿using AppMovilBase.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace AppMovilBase
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        private IList<LibroVM> libros = new ObservableCollection<LibroVM>();
        private LibroVMManager manager = new LibroVMManager();
        

        public MainPage()
        {
            BindingContext = libros;
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            GetListLibro();
        }

        async void GetListLibro()
        {
            try
            {
                var librosCollection = await manager.GetAll();
                BList.ItemsSource = null;
                BList.ItemsSource = new ObservableCollection<LibroVM>(librosCollection);
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
                return;
            }
        }

        async public void OnRefresh(object sender, EventArgs e)
        {
            var librosCollection = await manager.GetAll();
            foreach(LibroVM libro in librosCollection)
            {
                if(libros.All(l => l.Titulo != libro.Titulo))
                {
                    libros.Add(libro);
                }
                
            }
        }

        async public void OnAddLibro(object sender, EventArgs e)
        {
            LibroVM libro = new LibroVM();
            LibroVMManager libroVMManager = new LibroVMManager();
            await Navigation.PushAsync(new AddLibro(libro, libroVMManager));
        }

        async public void MenuItem_Clicked(object sender, EventArgs e)
        {
            try
            {
                var menu = sender as MenuItem;
                int Id = Convert.ToInt32(menu.CommandParameter.ToString());
                var reponseData = await manager.DeleteLibro(Id);
                if (reponseData.Status == 1)
                {
                    await DisplayAlert("Info", reponseData.Message, "OK");
                    GetListLibro();
                }
                else
                {
                    await DisplayAlert("Error", reponseData.Message, "OK");
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
                return;
            }
        }

        private async void List_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            try
            {
                LibroVM libroVM = e.Item as LibroVM;
                LibroVMManager libroVMManager = new LibroVMManager();
                await Navigation.PushAsync(new AddLibro(libroVM, libroVMManager));
            }
            catch(Exception ex)
            {
                await DisplayAlert("Error", ex.Message, "OK");
                return;
            }
        }

        private void ListRefresh(object sender, EventArgs e)
        {
            GetListLibro();
            BList.IsRefreshing = false;
        }
    }
}
